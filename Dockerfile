FROM debian:buster

RUN apt-get update \
  && apt-get install -qy \
  apt-transport-https \
  awscli \
  ca-certificates \
  curl \
  expect \
  gnupg2 \
  jq \
  make \
  software-properties-common \
  telnet \
  xmlstarlet \
  && curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - \
  && apt-key fingerprint 0EBFCD88 \
  && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" \
  && apt-get update \
  && apt-get install -qy \
  docker-ce \
  docker-ce-cli \
  containerd.io \
  sshpass \
  python3-ncclient \
  iproute2 \
  && apt-get -qy autoremove \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /root/.cache
